'use strict';
/* App module */

var prorustApp = angular.module('prorustApp',[
    // 'angular-loading-bar',
    'ui.router',
    'ui.bootstrap',
    // 'oc.lazyLoad',
    'afkl.lazyImage',
    'highcharts-ng',
    'xeditable',
    'prorustControllers',
    'prorustServices',
    'prorustDirectives'
]);


prorustApp.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
    function($stateProvider, $urlRouterProvider, $locationProvider){
        $urlRouterProvider.otherwise("/404");
        $stateProvider.
            state('/404', {
                title: 'Ошибка',
                url: "/404",
                templateUrl: "partials/404.html"
            }).
            state('/', {
                title: '',
                url: "/",
                templateUrl: "partials/main.html"
            }).
            state('/server', {
                title: 'Сервер',
                url: "/server",
                templateUrl: "partials/server.html",
                controller: 'ServerCtrl'
            }).
            state('/store', {
                title: 'Магазин',
                url: "/store",
                templateUrl: "partials/store.html",
                controller: 'StoreCtrl'
            }).
            state('/profile', {
                title: 'Профиль',
                url: "/profile",
                templateUrl: "partials/profile.html",
                controller: 'ProfileCtrl'
            }).             
            state('/profile/', {
                title: '',
                // url: "/profile/:InvId",
                url: "/profile/?pay",
                templateUrl: "partials/pay-status.html",
                controller: 'PayStatusCtrl'
            }).
            state('/logout', {
                title: 'Выход',
                url: "/logout",
                templateUrl: "partials/logout.html"
            }).
            state('/stat', {
                title: 'Статистика',
                url: "/stat",
                controller: 'StatCtrl',
                templateUrl: "partials/stat.html"
            }).
            state('/stat/', {
                title: 'Статистика Игрока',
                url: "/stat/:steamid",
                controller: 'StatProfileCtrl',
                templateUrl: "partials/stat-profile.html"
            }).
            state('/conditions', {
                title: 'Условия',
                url: "/conditions",
                templateUrl: "partials/conditions.html"
            }).
            state('/ts', {
                title: 'TeamSpeake',
                url: "/ts",
                templateUrl: "partials/ts.html"
            }).
            state('/test', {
                title: '',
                url: "/test",
                controller: 'TestCtrl',
                templateUrl: "partials/test.html",
            })
            ;
            
        // use the HTML5 History API
        $locationProvider.html5Mode(true);

    }
]);

prorustApp.run(['$location', '$rootScope', 'editableOptions', function($location, $rootScope, editableOptions) { 
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
}]);
