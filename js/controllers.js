var prorustControllers = angular.module('prorustControllers', []);

prorustControllers.controller('HeaderCtrl',['$scope', 'Server', '$location', '$state', '$rootScope', '$anchorScroll',
    function($scope, Server, $location, $state, $rootScope, $anchorScroll){
		$scope.isActive = function(viewLocation){
		    return viewLocation === $location.path();
		}
        $scope.gotoTop = function(){
            $location.hash('top');
            $anchorScroll();
        };

		$rootScope.user = [];
        $rootScope.modal = [];
		$scope.steamButtonShow = true;
        $scope.statMenuShow = false;

		Server.wq({q:'GetUserInfo'}).then(function(data){
			// console.log('GetUserInfo: '+JSON.stringify(data.data));
			if(data.data.length > 0){
				$rootScope.user['login'] = data.data[0]['login'];
    			$rootScope.user['steamid'] = data.data[0]['steamid'];
    			$rootScope.user['avatar'] = data.data[0]['avatar'];
    			$rootScope.user['balance'] = data.data[0]['balance'];
                all_pay = data.data[0]['all_pay'];
                $rootScope.user['all_pay'] = all_pay;
                $rootScope.user['all_trans'] = data.data[0]['all_trans'];
                $rootScope.user['discount'] = data.data[0]['discount'];

                $rootScope.user['discount_cur'] = data.data[0]['discount_cur'];
                $rootScope.user['discount_next'] = data.data[0]['discount_next'];
                $rootScope.user['all_pay_next'] = data.data[0]['all_pay_next'];
                $rootScope.user['progress_max'] = data.data[0]['progress_max'];
                $rootScope.user['progress_cur'] = data.data[0]['progress_cur'];


		$scope.steamButtonShow = false;

                $rootScope.user['stat'] = data.data[0]['stat'];
                $scope.statMenuShow = data.data[0]['stat'];

                // $rootScope.$broadcast('userupdate', '');//'' data

			}else if($location.path() == '/profile'){
                $state.go('/');
            }
            if($location.path() == '/logout'){
                $state.go('/store');
            }
            if($location.path() == '/stat' && $scope.statMenuShow === false){
                $state.go('/store');
            }
		});

		$scope.login = function(){
			window.open("https://prorust.ru/auth.php",'SteamAuth', 'width=950', 'height=480','resizable=no', 'scrollbars=no', 'status=yes');
		}

		$scope.logout = function(){
			Server.wq({q:'logout'}).then(function(data){
                $rootScope.user = [];
                $rootScope.modal = [];
				$scope.steamButtonShow = true;
                $scope.statMenuShow = false;
				$state.go('/logout');
			});
		}

    }//end function
]);

prorustControllers.controller('ProfileCtrl',['$scope', 'Server', 'AddTime',
    function($scope, Server, AddTime){
        $scope.robo = [];
        $scope.robo['mrh_login'] = null;
        $scope.robo['out_summ'] = 100;
        $scope.robo['inv_id'] = null;
        $scope.robo['inv_desc'] = null;
        // $scope.robo['shp_item'] = null;
        $scope.robo['crc'] = null;

        $scope.robo['in_curr'] = null;
        $scope.robo['culture'] = null;//'ru'
        $scope.robo['encoding'] = null;//'utf-8'

        $scope.profile = [];
        $scope.profile.all_summ_robo = 0;
        $scope.profile.all_summ_trans = 0;
        // $scope.dynamic = $scope.user['all_pay'];

        $scope.dynamic_bar = $scope.user['progress_cur'];
        $scope.max_bar = $scope.user['progress_max'];
        if($scope.user['all_pay'] >= 25000){
            $scope.dynamic = 25000;
        }else{
            $scope.dynamic = $scope.user['all_pay'];
        }
        $scope.max = $scope.user['all_pay_next'];

        // $scope.$on('userupdate', function (event) {//function (event, data) {
        //     console.log("userupdate: "+event);
        // });

        $scope.submit = function(){

            if($scope.user['steamid'] === '' || $scope.user['steamid'] === undefined){
                $scope.modal.msg = 'Пожалуйста авторизуйтесь!';
                $('#modalError').modal('show');
                return;
            }

            Server.wq({q:'RoboKassa', p1:$scope.user['login'], p2:$scope.user['steamid'], p3:$scope.robo['out_summ']}).then(function(data){
                $scope.robo = data.data;
                $scope.modal.msg = data.data.msg;
                if(data.data.msg == 'robokassa запись создана'){
                    document.getElementById("MrchLogin").value = $scope.robo['mrh_login'];
                    //document.getElementById("OutSum").value = $scope.robo['out_summ'];
                    document.getElementById("OutSum").value = $scope.robo['out_summ_float'];

                    document.getElementById("InvId").value = $scope.robo['inv_id'];
                    document.getElementById("Desc").value = $scope.robo['inv_desc'];
                    document.getElementById("SignatureValue").value = $scope.robo['crc'];
                    // document.getElementById("Shp_item").value = $scope.robo['shp_item'];
                    document.getElementById("IncCurrLabel").value = $scope.robo['in_curr'];
                    document.getElementById("Culture").value = $scope.robo['culture'];
                    document.getElementById("Encoding").value = $scope.robo['encoding'];

                    document.getElementById("subform").submit();
                }else if(data.data.msg == 'robokassa неправильная сумма'){
                    $('#modalError').modal('show');
                }                
            });

        }

        // $scope.unitpay_sum = 100;
        $scope.unitpay = [];
        $scope.unitpay['sum'] = 100;
        $scope.unitpay['desc'] = null;
        $scope.unitpay['sign'] = null;
        $scope.submitUnitpay = function(){
            
            if($scope.user['steamid'] === '' || $scope.user['steamid'] === undefined){
                $scope.modal.msg = 'Пожалуйста авторизуйтесь!';
                $('#modalError').modal('show');
                return;
            }

            Server.wq({q:'Unitpay', p1:$scope.user['steamid'], p2:$scope.unitpay['sum']}).then(function(data){
                $scope.unitpay = data.data;
                // console.log(data.data);
                document.getElementById("unitpay-account").value = $scope.unitpay['account'];
                document.getElementById("unitpay-sum").value = $scope.unitpay['sum'];
                document.getElementById("unitpay-desc").value = $scope.unitpay['desc'];
                document.getElementById("unitpay-sign").value = $scope.unitpay['sign'];

                document.getElementById("subFormUnitpay").submit();                
            });
        }

        $scope.feedback = [];
        $scope.modalfeedback = {
            show: function(){
                $('#modalFeedback').modal('show');
            },
            send: function(){
                $('#modalFeedback').modal('hide');
                $scope.modal.msg = 'Ваше обращение отправленно';
                $('#modalSuccess').modal('show');

                Server.wq({q:'feedback', p1:$scope.user['login'], p2:$scope.user['steamid'], p3:$scope.feedback['text'], p4:$scope.feedback['email']}).then(function(data){
                    //console.log(data);//feedback-ready
                });
            }
        }

        //TAB История платежей
        $scope.userPayments = [];
        $scope.getUserPayments = function(){
            $scope.profile.all_summ_robo = 0;
            Server.wq({q:'GetUserPayments', p1:$scope.user['steamid']}).then(function(data){
                if (data.data != 'null'){
                    $scope.userPayments = data.data;
                    $scope.roboTabShow = true;
                    for (var i = 0; i < $scope.userPayments.length; i++) {
                        $scope.profile.all_summ_robo += Math.floor($scope.userPayments[i].out_summ);//$scope.mathFloor($scope.userPayments[i].out_summ);
                    };
                    // console.log($scope.profile.all_summ_robo);
                }
            });//end wq GetUserPayments            
        }
        $scope.mathFloor = function(out_summ){
            return Math.floor(out_summ);
        }
        $scope.addtime = function(unix_timestamp){
            return AddTime.conv(unix_timestamp);
        }
        // $scope.pay_logo = function(s_referer){
        //     if(s_referer == 'robokassa'){
        //         return "../img/robo-logo.png";
        //     }else if(s_referer == 'unitpay'){
        //         return "../img/unitpay-logo.png";
        //     }
        // }

        //TAB История покупок
        $scope.userTransactions = [];
        $scope.getUserTransactions = function(){
            $scope.profile.all_summ_trans = 0;
            Server.wq({q:'GetUserTransactions', p1:$scope.user['steamid']}).then(function(data){
                if (data.data != 'null'){
                    $scope.userTransactions = data.data;
                    $scope.transTabShow = true;
                    for (var i = 0; i < $scope.userTransactions.length; i++) {
                        // $scope.profile.all_summ_trans += Math.floor($scope.userTransactions[i].summ);
                        $scope.profile.all_summ_trans += $scope.userTransactions[i].summ;
                    };
                }
            });//end wq GetUserTransactions 
        }
        $scope.item_type = function(dat){
            if(!dat.is_kit && !dat._is_bp){
                return "Предмет (item)";
            }else 
            if(!dat.is_kit && $dat.is_bp){
                return "Рецепт (blueprint)";
            }else 
            if(dat.is_kit){
                return "Набор (kit)";
            }

        }

    }
]);

prorustControllers.controller('ServerCtrl',['$scope', 'Server',
    function($scope, Server){
        $scope.players = [];
        Server.wq({q:'GetUsersOnline'}).then(function(data){
            $scope.count = data.data['Online']['Count'];
            var objects = data.data['Online']['Players'];
            for(var key in objects) {
                var value = objects[key];
                $scope.players.push(value);
            }
            $scope.loadingShow = false;
            $('#modalLoading').modal('hide');
        });
        Server.wq({q:'GetUsersOnline2'}).then(function(data){
            $scope.count2 = data.data['Online']['Count'];
        });
    }
]);

prorustControllers.controller('StoreCtrl',['$scope', 'Server', 'ItemCollection', 'Discount',
    function($scope, Server, ItemCollection, Discount){

        $scope.items = [];
        $scope.storeHide = true;

        if(ItemCollection.getitems() === undefined){
            $scope.loadingShow = true;
            $('#modalLoading').modal('show');
            Server.wq({q:'GetItems'}).then(function(data){
                if(data.data.error){
                    $scope.modal.msg = data.data.msg;
                    $('#modalError').modal('show');
                    $scope.loadingShow = false;
                    $('#modalLoading').modal('hide');
                }else{                    
                    ItemCollection.setitems(data.data);
                    $scope.items = ItemCollection.getitems();
                    $scope.loadingShow = false;
                    $('#modalLoading').modal('hide');
                    $scope.storeHide = false;
                }
            });
        }else{
            $scope.storeHide = false;
            $scope.items = ItemCollection.getitems();
            // console.log("$scope.items: "+$scope.items);
        }

        $scope.discountPrice = function(price){
            return Discount.price(price, $scope.user.discount);
        }

        $scope.curr_item = [];
        $scope.modal.msg = '';
        $scope.buy = function(item){
            if($scope.user.steamid !== undefined){
                $scope.curr_item = item;
                $scope.curr_item.count = 1;
                $scope.curr_item.curr_price = item.price;
                $scope.curr_item.curr_quantity = item.quantity;
                $scope.curr_item.need_slots = item.number;

                if($scope.discountPrice($scope.curr_item.curr_price) <= $scope.user.balance){
                    $('#modalBuyItem2').modal('show');
                }else{
                    $scope.modal.msg = $scope.curr_item.name+' - недостаточно средств!';
                    $('#modalError').modal('show');
                }
            }else{
                $scope.modal.msg = 'Пожалуйста авторизуйтесь!';
                $('#modalError').modal('show');
            }            
        };
        $scope.buyok = function(item){
            $('#modalBuyItem2').modal('hide');
            $('#modalProgress').modal('show');

            Server.wq({q:'BuyItem', p1:$scope.user.steamid, p2:item.id, p3:$scope.curr_item.count}).then(function(data){
                $('#modalProgress').modal('hide');
                console.log(data.data);///////////////
                if(data.data.error){
                    $scope.modal.msg = 'Ошибка: '+data.data.msg;
                    $('#modalError').modal('show');
                }else{
                    $scope.user.balance = data.data.balance;
                    $scope.modal.msg = data.data.msg;
                    $('#modalSuccess').modal('show');
                }             
            });
        };
        $scope.customFilter = function(param){
            return function(item){
                if(param == 'all'){
                    return item;
                }else{
                    return item.type == param;
                }
            }
        };
        $scope.quantityMinus = function(){
            if($scope.curr_item.count > 1){
                $scope.curr_item.count -= 1;
                $scope.curr_item.curr_price = $scope.curr_item.price * $scope.curr_item.count;
                $scope.curr_item.curr_quantity = $scope.curr_item.quantity * $scope.curr_item.count;
                $scope.curr_item.need_slots = Math.ceil( $scope.curr_item.curr_quantity / $scope.curr_item.stack_size );
            }
        }
        $scope.quantityPlus = function(){
            $scope.curr_item.count += 1;
            $scope.curr_item.curr_price = $scope.curr_item.price * $scope.curr_item.count;
            $scope.curr_item.curr_quantity = $scope.curr_item.quantity * $scope.curr_item.count;
            $scope.curr_item.need_slots = Math.ceil( $scope.curr_item.curr_quantity / $scope.curr_item.stack_size );
        }

    }//end function
]);//end StoreCtrl

prorustControllers.controller('CarouselCtrl',['$scope',
    function($scope){
        $scope.myInterval = 4000;
        var slides = $scope.slides = [];
        slides.push(
        {
            image: 'img/slider/kage.jpg',
            text: ['test']
        },
        {
            image: 'img/slider/bard.jpg',
            text: ['test']
        },
        {
            image: 'img/slider/fire.jpg',
            text: ['test']
        },
        {
            image: 'img/slider/home.jpg',
            text: ['test']
        },
        {
            image: 'img/slider/ass.jpg',
            text: ['test']
        }
        );
    }
]);

prorustControllers.controller('PayStatusCtrl',['$scope', '$stateParams', '$state',
    function($scope, $stateParams, $state){
        $scope.status = '';
        if( $stateParams.pay === 'success' ){
            $scope.status = 'Платеж выполнен успешно!';
        }else if( $stateParams.pay === 'error' ){
            $scope.status = 'Во время выполнения платежа произошла ошибка!';
        }
    }
]);


prorustControllers.controller('StatCtrl',['$scope', 'ServerStat', '$filter',
    function($scope, ServerStat, $filter){

        $scope.allsum = 0;

        $scope.chartConfigRobokassa = {
            options: {
                chart: {
                    zoomType: 'x',
                    type: 'spline',
                    backgroundColor: 'black',
                },
                rangeSelector: {
                    enabled: true
                },
                navigator: {
                    enabled: true
                }
            },
            series: [],
            loading: true,
            title: {
                text: 'Chart'
            },
            yAxis: {
                min: 0,
                title: {text: 'rur'}
            },
            useHighStocks: true
        };

        $scope.chartRobokassa = function(){
            ServerStat.wq({q:'GetRobokassa'}).then(function(data){
                $scope.robokassaData = data.data;
                var mySeries = [];
                var prevdate = new Date((data.data[0]['date'])*1000);

                var sum = 0;
                var allsum = 0;

                for (var i = 0; i < $scope.robokassaData.length; i++) {                    
                    var date = new Date((data.data[i]['date'])*1000);
                    allsum += Math.floor($scope.robokassaData[i]['out_summ']);                    
                    if(date.getDate() == prevdate.getDate()){
                        sum += Math.floor($scope.robokassaData[i]['out_summ']);
                    }else{
                        dateObj = new Date(prevdate.getFullYear(), prevdate.getMonth(), prevdate.getDate()+1);
                        mySeries.push([ dateObj.getTime(), sum ]);
                        sum = Math.floor($scope.robokassaData[i]['out_summ']);
                        prevdate = date;
                    }
                    if(i == $scope.robokassaData.length-1){
                        dateObj = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1);
                        mySeries.push([ dateObj.getTime(), sum ]);
                    }                    
                }
                $scope.allsum = allsum;
                $scope.chartConfigRobokassa.series.push({
                    id: 1,
                    color: '#ce432c',
                    data: mySeries.reverse()//$scope.chartdata
                });
                $scope.chartConfigRobokassa.loading = false;
                
                $scope.chartUsers();

            });//end ServerStat.wq
        }
        $scope.chartRobokassa();
       

        //Users
        $scope.chartConfigUsers = {
            options: {
                chart: {
                    height: 6000,
                    type: 'bar',
                    backgroundColor: 'black',
                },
                rangeSelector: {
                    enabled: false
                },
                navigator: {
                    enabled: false
                }
            },
            series: [],
            title: {
                text: 'Chart'
            },
            loading: true,
            xAxis: {
                categories: [],
                title: {text: 'Users'}
            },
            yAxis: {
                title: {text: 'donate rur'}
            },
            useHighStocks: false
        };

        $scope.usersSeries = [];
        $scope.chartUsers = function(){
            var mySeries = [];
            var chartUsers = [];
            var chartUsersSumm = [];
            var chartUsersSpent = [];

            $scope.usersSeries = [];
            for (var i = 0; i < $scope.robokassaData.length; i++) {
                login = JSON.stringify($scope.robokassaData[i].login);
                steamid = $scope.robokassaData[i].steamid;
                if(mySeries[ steamid ]  === undefined){
                    mySeries.push(steamid);
                    mySeries[ steamid ] = { login: login, out_summ: Math.floor($scope.robokassaData[i].out_summ), spent:0 };
                }else{
                    mySeries[ steamid ].out_summ += Math.floor($scope.robokassaData[i].out_summ);
                }
            }
            $scope.usersSeries = mySeries;
            $scope.chartTransactions();
        }
        //end Users

        //Transactions
        $scope.chartConfigTransactions = {
            options: {
                chart: {
                    height: 1800,
                    type: 'bar',
                    backgroundColor: 'black',
                },
                rangeSelector: {
                    enabled: false
                },
                navigator: {
                    enabled: false
                }
            },
            series: [],
            title: {
                text: 'Chart Transactions'
            },
            loading: true,
            xAxis: {
                categories: [],
                title: {text: 'Items'}
            },
            yAxis: {
                title: {text: 'rur'}
            },
            useHighStocks: false
        };

        $scope.noneRobokassa = [];
        $scope.chartTransactions = function(){
            ServerStat.wq({q:'GetTransactions'}).then(function(data){
                var noneRobo = [];
                var transactionsData = data.data;
                var mySeries = [];
                var myItems = [];
                var mySumm = [];
                var myCount = [];

                var mySeriesSort = [];
                var j = -1;
                for (var i = 0; i < transactionsData.length; i++) {
                    if(transactionsData[i].refunded == 0){

                        if(mySeries[ transactionsData[i].item_name ] === undefined){
                            mySeries.push(transactionsData[i].item_name);
                            mySeries[ transactionsData[i].item_name ] = 0;
                            mySeriesSort.push({ name: transactionsData[i].item_name, summ: transactionsData[i].summ, count:1 });
                            j++;
                        }else{
                            mySeriesSort[j].summ += transactionsData[i].summ;
                            mySeriesSort[j].count += 1;
                        }

                        var steamid = transactionsData[i].steamid;

                        if($scope.usersSeries[steamid] != undefined){
                            $scope.usersSeries[steamid].spent += transactionsData[i].summ;
                        }
                        if($scope.usersSeries[steamid] == undefined){
                            if(noneRobo[steamid] == undefined){
                                noneRobo.push(steamid);
                                noneRobo[steamid] = { steamid:steamid, summ:transactionsData[i].summ, login: transactionsData[i].login, datetime:transactionsData[i].datetime};
                            }else{
                                noneRobo[steamid].summ += transactionsData[i].summ;
                            }

                        }
                    }
                }
                // $scope.noneRobokassa = noneRobo;
                // console.log(noneRobo[0]);
                for (var i = 0; i < noneRobo.length; i++) {
                    $scope.noneRobokassa.push(noneRobo[ noneRobo[i] ]);
                };

                mySeriesSort.sort(function (a, b) {
                    if (a.summ > b.summ) {
                        return -1;
                    }
                    if (a.summ < b.summ) {
                        return 1;
                    }
                    // a должно быть равным b
                    return 0;
                });
                for(var i=0; i<mySeriesSort.length; i++){
                    myItems.push( mySeriesSort[i].name );
                    mySumm.push( mySeriesSort[i].summ );
                    myCount.push( mySeriesSort[i].count );
                }
                $scope.chartConfigTransactions.series.push({
                    id: 1,
                    color: '#ce432c',
                    name: 'buy item',
                    data: mySumm
                },{
                    id: 2,
                    color: 'orange',
                    name: 'count',
                    data: myCount
                }
                );
                // console.log(JSON.stringify(myCount));                
                $scope.chartConfigTransactions.xAxis.categories = myItems;
                $scope.chartConfigTransactions.loading = false;

                //chartUsers CONTINUOS ...
                var chartUsers = [];
                var chartUsersSumm = [];
                var chartUsersSpent = [];
                for(var i=0; i<$scope.usersSeries.length; i++){
                    chartUsers.push( $scope.usersSeries[ $scope.usersSeries[i] ].login );
                    chartUsersSumm.push( $scope.usersSeries[ $scope.usersSeries[i] ].out_summ );
                    chartUsersSpent.push( $scope.usersSeries[ $scope.usersSeries[i] ].spent );
                }
                $scope.chartConfigUsers.series.push({
                    id: 1,
                    color: '#ce432c',
                    data: chartUsersSumm,
                    name: 'user donate'
                },{
                    id: 2,
                    color: 'orange',
                    name: 'spent',
                    data: chartUsersSpent
                });
                $scope.chartConfigUsers.xAxis.categories = chartUsers;
                $scope.chartConfigUsers.loading = false;
                //chartUsers end CONTINUOS

            });//end ServerStat.wq
        }
        //end Transactions

        ////////////////////All Users
        $scope.getAllUsers = function(){
            ServerStat.wq({q: 'GetAllUsers'}).then(function(data) {
                $scope.allUsers = data.data;
            });
        }
        $scope.searchUser = function(val) {
            if(val.length > 3){
                return ServerStat.wq({q:'SearchUsers', search_val:val}).then(function(response){
                    data = response.data;
                    if(!data['error']){
                        return response.data.map(function(item){
                            return item;
                        });
                    }else{
                        $scope.modal.msg = 'Ошибка: '+data.msg;
                        $('#modalError').modal('show');
                    }
                });                
            }
        }
        ////////////////////end All Users

        ////////////////////MoneyGifts
        $scope.getMoneyGifts = function(){
            ServerStat.wq({q: 'GetMoneyGifts'}).then(function(data) {
                $scope.MoneyGifts = data.data;
            });
        }
        ////////////////////end MoneyGifts

        //Banlist
        $scope.users = [];
        $scope.statuses = [
            {value: 0, text: 'разбан.'},
            {value: 1, text: 'Забанен'}
        ];

        $scope.showStatus = function(user) {
            var selected = $filter('filter')($scope.statuses, {value: user});
            // return (user && selected.length) ? selected[0].text : 'Not set';
            return selected.length ? selected[0].text : $scope.statuses[0].text;//'Not set';
        };

        $scope.getBanlist = function(){
            ServerStat.wq({q: 'GetBanlist'}).then(function(data) {
                $scope.users = data.data;
            });
        }
        // $scope.getBanlist();

        $scope.updateData = function(user, column, state) {
            ServerStat.wq({q:'setStateBanlist', id:user.id, column:column, state:state}).then(function(data){
                //console.log(data.data);//console.log(JSON.stringify(data));//success
                data = data.data;
                user.referee = data['referee'];
            });
        };

        // add user
        $scope.addUser = function() {
            $scope.inserted = {
                //id: $scope.users.length + 1,
                id: '',
                login: '',
                steamid: '',
                comment: '',
                bantime: '',
                referee: '',
                status: 1,
            };
            ServerStat.wq({q:'addUserBanlist'}).then(function(data) {
                // $scope.inserted['id'] = JSON.parse(data.data['last_id']);
                data = data.data;
                $scope.inserted['id'] = data['last_id'];
                $scope.inserted['bantime'] = data['bantime'];
                $scope.inserted['referee'] = data['referee'];

                $scope.users.push($scope.inserted);
            }); //end ServerStat.wq
        };

        $scope.delUser = function(user) {
            index = $scope.users.indexOf(user);
            ServerStat.wq({q:'delUserBanlist', id:user.id}).then(function(data){
            if(data.data == '"delUserBanlist:success"'){
                $scope.users.splice(index, 1);
            }else{
                console.log(data.data);//delChannel:error
            }
            
            });
        };
        //end Banlist

        //////////////////////////Expenses
        $scope.getExpenses = function(){
            $scope.expenses = [];
            $scope.expenses_all = 0;
            ServerStat.wq({q: 'GetExpenses'}).then(function(data) {
                $scope.expenses = data.data;                
                for (var i = 0; i < $scope.expenses.length; i++) {
                    $scope.expenses_all += $scope.expenses[i].summ;
                };
            });
        }
        // $scope.getBanlist();

        $scope.updateDataExpenses = function(expense, column, state) {
            ServerStat.wq({q:'setStateExpenses', id:expense.id, column:column, state:state}).then(function(data){
                //console.log(data.data);//console.log(JSON.stringify(data));//success
                data = data.data;
                expense.login = data['login'];
                if(column == 'summ'){
                    $scope.expenses_all = 0;
                    for (var i = 0; i < $scope.expenses.length; i++) {
                        $scope.expenses_all += parseInt($scope.expenses[i].summ);
                    };
                }
            });
        };

        // add expense
        $scope.addExpense = function() {
            $scope.insertedExpense = {
                //id: $scope.users.length + 1,
                id: '',
                summ: '',
                comment: '',
                login: '',
                steamid: '',
                datetime: ''
            };
            ServerStat.wq({q:'addExpense'}).then(function(data) {
                // $scope.inserted['id'] = JSON.parse(data.data['last_id']);
                data = data.data;
                $scope.insertedExpense['id'] = data['last_id'];
                $scope.insertedExpense['datetime'] = data['datetime'];
                $scope.insertedExpense['login'] = data['login'];

                $scope.expenses.push($scope.insertedExpense);
            }); //end ServerStat.wq
        };

        $scope.delExpense = function(expense) {
            index = $scope.expenses.indexOf(expense);
            ServerStat.wq({q:'delExpense', id:expense.id}).then(function(data){
                if(data.data == '"delExpense:success"'){
                    $scope.expenses.splice(index, 1);
                    $scope.expenses_all = 0;
                    for (var i = 0; i < $scope.expenses.length; i++) {
                        $scope.expenses_all += parseInt($scope.expenses[i].summ);
                    };
                }else{
                    console.log(data.data);//delChannel:error
                }            
            });
        };
        //////////////////////////end Expenses

        //////////////////////////Settings
        $scope.getSettings = function(){
            $scope.settings = [];
            ServerStat.wq({q: 'GetSettings'}).then(function(data) {
                $scope.settings = data.data; 
            });
        }
        $scope.store_stat = [
            {value: 'on', text: 'Включен'},
            {value: 'off', text: 'Выключен'}
        ];

        $scope.script_exec_stat = [
            {value: 'rustcmd.php', text: 'rustcmd.php'},
            {value: 'add_item.php', text: 'add_item.php'}
        ];

        $scope.showSettings = function(user, arr) {
            var selected = $filter('filter')(arr, {value: user});
            return selected.length ? selected[0].text : $scope.arr[0].text;//'Not set';
        };

        $scope.setSettings = function(column, state) {
            ServerStat.wq({q:'SetSettings', server:'prorust', column:column, state:state}).then(function(data){
            });
        };
        //
        //////////////////////////end Settings

        ////////////////////////////////////////
        //
        $scope.mathFloor = function(out_summ){
            return Math.floor(out_summ);
        }
        $scope.addtime = function(unix_timestamp){
            var date = new Date(unix_timestamp*1000);
            var D = (date.getDate() > 9 ? date.getDate() : '0'+date.getDate());
            var M = ((date.getMonth()+1) > 9 ? (date.getMonth()+1) : '0'+(date.getMonth()+1));
            var Y = (date.getFullYear());
            var hh = (date.getHours() > 9 ? date.getHours() : '0'+date.getHours());
            var mm = (date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes());
            var ss = (date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds());
            return hh+':'+mm+':'+ss+' '+D+'.'+M+'.'+Y;
            //return (date.getDate() > 9 ? date.getDate() : '0'+date.getDate())+'.'+(date.getMonth()+1 > 9 ? date.getMonth()+1 : ''+date.getMonth()+1)+'.'+(date.getFullYear())+' '+(date.getHours() > 9 ? date.getHours() : '0'+date.getHours())+':'+(date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes())+':'+(date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds());
        };
        //
        ////////////////////////////////////////
        
    }//end
]);

prorustControllers.controller('StatProfileCtrl',['$scope', '$stateParams', 'ServerStat', '$window', 'AddTime',
    function($scope, $stateParams, ServerStat, $window, AddTime){
        $scope.profile = [];
        $scope.getUserInfo = function(){
            ServerStat.wq({q:'GetUserInfo', p1:$stateParams.steamid}).then(function(data){
                // console.log('GetUserInfo: '+JSON.stringify(data.data));
                if(data.data.length > 0){
                    $scope.profile['login'] = data.data[0]['login'];
                    $scope.profile['steamid'] = data.data[0]['steamid'];
                    $scope.profile['avatar'] = data.data[0]['avatar'];
                    $scope.profile['balance'] = data.data[0]['balance'];
                    $scope.profile['all_pay'] = data.data[0]['all_pay'];
                    $scope.profile['all_trans'] = data.data[0]['all_trans'];
                    $scope.profile['discount'] = data.data[0]['discount'];

                    $scope.profile['discount_cur'] = data.data[0]['discount_cur'];
                    $scope.profile['discount_next'] = data.data[0]['discount_next'];
                    $scope.profile['all_pay_next'] = data.data[0]['all_pay_next'];
                    $scope.profile['progress_max'] = data.data[0]['progress_max'];
                    $scope.profile['progress_cur'] = data.data[0]['progress_cur'];

                    $scope.dynamic_bar = $scope.profile['progress_cur'];
                    $scope.max_bar = $scope.profile['progress_max'];
                    if($scope.profile['all_pay'] >= 25000){
                        $scope.dynamic = 25000;
                    }else{
                        $scope.dynamic = $scope.profile['all_pay'];
                    }
                    $scope.max = $scope.profile['all_pay_next'];


                }
            });//end wq GetUserInfo
        }
        $scope.getUserInfo();

        $scope.roboData = [];
        $scope.profile.all_summ_robo = 0;
        ServerStat.wq({q:'GetRobokassa', p1:$stateParams.steamid}).then(function(data){
            if (data.data != 'null'){
                $scope.roboData = data.data;
                $scope.roboTabShow = true;
                $scope.profile.all_summ_robo = $scope.roboData[0].all_summ_robo;

            }
        });//end wq GetRobokassa
        $scope.transData = [];
        $scope.getTransactions = function(){
            $scope.profile.all_summ_trans = 0;
            ServerStat.wq({q:'GetTransactions', p1:$stateParams.steamid}).then(function(data){
                if (data.data != 'null'){
                    $scope.transData = data.data;
                    $scope.transTabShow = true;
                    $scope.profile.all_summ_trans = $scope.transData[0].all_summ_trans;
                }
            });//end wq GetTransactions            
        }

        $scope.getTransactions();

        ServerStat.wq({q:'getState', table:'items', id:'78'}).then(function(data){
            $scope.resetBPprice = data.data.price;
        });//end wq getState

        $scope.delTrans = function(dat) {
            index = $scope.transData.indexOf(dat);
            ServerStat.wq({q:'DelTransaction', p1:$stateParams.steamid, item_id:dat.id}).then(function(data){
                if(data.data.error){
                    $scope.modal.msg = 'Ошибка: '+data.data.msg;
                    $('#modalError').modal('show');
                }else{
                    $scope.transData.splice(index, 1);
                    $scope.profile.balance = data.data.balance;
                    $scope.profile.all_summ_trans -= dat.summ;
                }
            });
        };

        $scope.resetBP = function(){
            ServerStat.wq({q:'resetBP', p1:$stateParams.steamid}).then(function(data){
                data = data.data;
                // console.log(data);
                if(!data['error']){
                    $scope.modal.msg = data.msg;
                    $('#modalSuccess').modal('show');

                    $scope.getUserInfo();
                    $scope.getTransactions();

                }else{
                    $scope.modal.msg = 'Ошибка: '+data.msg;
                    $('#modalError').modal('show');
                }
            });//end wq resetTransactions
        }

        $scope.mgData = [];
        $scope.mg_summ = 0;
        $scope.profile.all_summ_mg = 0;
        ServerStat.wq({q:'GetMoneyGifts', p1:$stateParams.steamid}).then(function(data){
                // console.log('data: '+JSON.stringify(data.data));
            if (data.data != 'null'){
                $scope.mgData = data.data;
                $scope.mgTabShow = true;
                for (var i = 0; i < $scope.mgData.length; i++) {
                    $scope.profile.all_summ_mg += $scope.mgData[i].summ;
                };
            }
        });//end wq GetMoneyGifts

        $scope.minusMoneyGifts = function(summ){
            $scope.plusMoneyGifts(-summ);
        }
        $scope.plusMoneyGifts = function(summ) {
            $scope.insertedMoneyGifts = {
                id: '',
                from_login: '',
                from_steamid: '',
                summ: '',
                to_login: '',
                to_steamid: '',
                datetime: ''
            };
            if( ($scope.profile['steamid'] != undefined) && ($scope.profile['login'] != undefined) && (summ != 0) ){
                ServerStat.wq({q:'addMoneyGifts', p1:$scope.profile['steamid'], p2:$scope.profile['login'], p3:summ}).then(function(data) {
                    data = data.data;
                    $scope.insertedMoneyGifts['id'] = data['last_id'];
                    $scope.insertedMoneyGifts['from_login'] = data['from_login'];
                    $scope.insertedMoneyGifts['from_steamid'] = data['from_steamid'];
                    $scope.insertedMoneyGifts['summ'] = data['summ'];
                    $scope.insertedMoneyGifts['to_login'] = data['to_login'];
                    $scope.insertedMoneyGifts['to_steamid'] = data['to_steamid'];
                    $scope.insertedMoneyGifts['datetime'] = data['datetime'];

                    $scope.profile['balance'] += data['summ'];
                    $scope.profile.all_summ_mg += data['summ'];

                    $scope.mgData.push($scope.insertedMoneyGifts);
                }); //end ServerStat.wq                
            }
        };
        $scope.updateDataMG = function(dat, column, state) {
            ServerStat.wq({q:'setState', table:'money_gifts', id:dat.id, column:column, state:state}).then(function(data){
            });
        };
        $scope.react = function(dat){
            ServerStat.wq({q:'react', trans_id:dat.id}).then(function(data){
                data = data.data;
                if(!data['error']){
                    dat.react = data.react;
                    dat.datetime = data.t;
                    $scope.modal.msg = data.msg;
                    $('#modalSuccess').modal('show');
                }else{
                    $scope.modal.msg = 'Ошибка: '+data.msg;
                    $('#modalError').modal('show');
                }
            });
        }
        $scope.refund = function(dat){
            ServerStat.wq({q:'Refund', p1:$stateParams.steamid, trans_id:dat.id}).then(function(data){
                data = data.data;
                if(!data['error']){
                    dat.refunded = data.refunded;
                    $scope.profile.balance = data.balance;
                    $scope.profile.all_summ_trans -= dat.summ;
                    $scope.modal.msg = data.msg;
                    $('#modalSuccess').modal('show');
                }else{
                    $scope.modal.msg = 'Ошибка: '+data.msg;
                    $('#modalError').modal('show');
                }
            });
        }
        $scope.unrefund = function(dat){
            ServerStat.wq({q:'UnRefund', p1:$stateParams.steamid, trans_id:dat.id}).then(function(data){
                data = data.data;
                if(!data['error']){
                    dat.refunded = data.refunded;
                    $scope.profile.balance = data.balance;
                    $scope.profile.all_summ_trans += dat.summ;
                    $scope.modal.msg = data.msg;
                    $('#modalSuccess').modal('show');
                }else{
                    $scope.modal.msg = 'Ошибка: '+data.msg;
                    $('#modalError').modal('show');
                }
            });
        }
        $scope.closeProfile = function(){
            $window.close();
        }
        ////////////////////////////////////////
        //
        $scope.mathFloor = function(out_summ){
            return Math.floor(out_summ);
        }
        $scope.addtime = function(unix_timestamp){
            return AddTime.conv(unix_timestamp);
        }
        //
        ////////////////////////////////////////
    }//end function
]);//end StatProfileCtrl

prorustControllers.controller('TestCtrl',['$scope',
    function($scope){

    }
]);
