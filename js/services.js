var prorustServices = angular.module('prorustServices',[]);

prorustApp.factory('Server',['$http',
    function($http){
		return {
		    wq: function(q){
		    	// q.city = $rootScope.city;
		    	return $http.post('backend/server.php', q).then(function(res){
				    return res;
				});
		    }
		}
    }
]);

prorustApp.service('ItemCollection',[
    function(){
        return {
            getitems: function(){
                return this.items;
            },
            setitems: function(items){
                this.items = items;
            }
        }
    }
]);

prorustApp.factory('ServerStat',['$http',
    function($http){
        return {
            wq: function(q){
                // q.city = $rootScope.city;
                return $http.post('backend/stat.php', q).then(function(res){
                    return res;
                });
            }
        }
    }
]);

prorustApp.service('AddTime',[
    function(){
        return {
            conv: function(unix_timestamp){
                var date = new Date(unix_timestamp*1000);
                var D = (date.getDate() > 9 ? date.getDate() : '0'+date.getDate());
                var M = ((date.getMonth()+1) > 9 ? (date.getMonth()+1) : '0'+(date.getMonth()+1));
                var Y = (date.getFullYear());
                var hh = (date.getHours() > 9 ? date.getHours() : '0'+date.getHours());
                var mm = (date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes());
                var ss = (date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds());
                return hh+':'+mm+':'+ss+' '+D+'.'+M+'.'+Y;
                //return (date.getDate() > 9 ? date.getDate() : '0'+date.getDate())+'.'+(date.getMonth()+1 > 9 ? date.getMonth()+1 : ''+date.getMonth()+1)+'.'+(date.getFullYear())+' '+(date.getHours() > 9 ? date.getHours() : '0'+date.getHours())+':'+(date.getMinutes() > 9 ? date.getMinutes() : '0'+date.getMinutes())+':'+(date.getSeconds() > 9 ? date.getSeconds() : '0'+date.getSeconds());
            }
        }
    }
]);

prorustApp.service('Discount',[
    function(){
        return {
            price: function(price, discount){
		var priceDiscount = price;

                if(discount > 0){
                    priceDiscount = price - (price / 100 * discount);

                }
                return priceDiscount;
            }

        }
    }
]);